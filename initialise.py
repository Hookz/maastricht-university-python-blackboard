from blackboard import *
from datetime import datetime, timedelta
import re

now = datetime.now()
current_period = 0


def get_current_period():
    if current_period == 0:
        thisyear = now.year-1
        if now.month >= 9:
            thisyear = now.year
        first_monday_of_p1 = datetime.strptime(str(thisyear)+' 35 1', '%Y %W %w')
        last_friday_of_p1 = first_monday_of_p1 + timedelta(days=53)  # 8 weeks - 3 days
        first_monday_of_p2 = last_friday_of_p1 + timedelta(days=3)
        last_friday_of_p2 = last_friday_of_p1 + timedelta(days=56)
        # Christmas Break
        first_monday_of_p3 = datetime.strptime(str(thisyear+1)+' 1 1', '%Y %W %w')
        last_friday_of_p3 = first_monday_of_p3 + timedelta(days=25)
        first_monday_of_p4 = last_friday_of_p3 + timedelta(days=3)
        last_friday_of_p4 = last_friday_of_p3 + timedelta(days=63)
        first_monday_of_p5 = last_friday_of_p4 + timedelta(days=3)
        last_friday_of_p5 = last_friday_of_p4 + timedelta(days=63)
        first_monday_of_p6 = last_friday_of_p5 + timedelta(days=3)
        last_friday_of_p6 = last_friday_of_p5 + timedelta(days=28)
        if now <= last_friday_of_p1:
            return "100"
        elif now <= last_friday_of_p2:
            return "200"
        elif now <= last_friday_of_p3:
            return "300"
        elif now <= last_friday_of_p4:
            return "400"
        elif now <= last_friday_of_p5:
            return "500"
        elif now <= last_friday_of_p6:
            return "600"
        return "100"
    return current_period


blackboard = Blackboard("iXXXXXXX", "PASSWORD")  # Exchange for your credentials
courses = blackboard.get_current_courses()
current_courses = []
courses_class_list = []
for course in courses:
    if not course.organisation:
        if re.search("["+str(now.year - 1) + "]?" + str(now.year) + "-"+get_current_period()+"-\\w*", course.name):
            current_courses += [course]


class CurrentCourses:
    def __init__(self):
        self.courses = current_courses


def parse_courses(courses_to_parse):
    for a_course in courses_to_parse:
        print(a_course.name)

