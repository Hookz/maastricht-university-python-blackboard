"""
This script downloads all contents of a course into a directory tree.
"""
from sys import argv
from pathlib import Path

from blackboard import *

if len(argv) != 2:
    print("""
Usage:
    get_entire_course.py <course name>
""")
    exit(1)

COURSE_NAME = argv[1].strip()

with open('creds') as file_:
    username, password = [l.strip() for l in file_.readlines()]

def save_file(filename, content):
    print("Saving file {}".format(str(filename)))
    # are we dealing with a string or raw bytes?
    if hasattr(content, 'encode'):
        mode = 'w'
    else:
        mode = 'wb'
    with open(filename, mode=mode) as file_:
        file_.write(content)

def pull_content(parent_dir, content):
    path = Path(parent_dir, content.title)
    path.mkdir(parents=True, exist_ok=True)
    print("Looking into {}".format(str(path)))
    if 'body' in content:
        save_file(path.joinpath('body.html'), content.body)
    try:
        for attachment in content.attachments:
            save_file(path.joinpath(attachment.fileName), attachment.download())
    except ConnectionException:
        pass
    if content.hasChildren:
        for child in content.children:
            pull_content(path, child)

def pull_course(parent_dir, course):
    path = Path(parent_dir, course.name)
    path.mkdir(parents=True, exist_ok=True)
    for child in course.children:
        pull_content(path, child)

OUTPUT_DIR = 'out'

blackboard = Blackboard(username, password)
course = blackboard.get_courses({'name': COURSE_NAME})[0]

pull_course(OUTPUT_DIR, course)
