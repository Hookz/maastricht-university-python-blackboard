# Maastricht university python Blackboard (unofficial)
Simple package to retrieve information from Blackboard.

## Example
This allows you to make scripts that: notify you when a new Lecture/Assignment has been uploaded, addsg deadlines automatically in your calendar, automatically downloads assignment material on your computer.
The possibilities are endless.
To get started, make a file `creds` with your username on the first line and password on second, and then jump into the REPL and simply write:
```
from blackboard import *
with open('creds') as file_:
    username, password = [l.strip() for l in file_.readlines()]
blackboard = Blackboard(username, password)
course = blackboard.get_courses({'name': 'Theoretical Computer Science (2019-2020-400-KEN2420)'})[0]
print(course.children[0].children[0])
```

## TODO
- [ ] This README file
- [ ] Documentation
- [ ] Handling error messages
- [ ] Add more features 
